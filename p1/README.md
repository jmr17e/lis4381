> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Julia Riccio

### Project 1 Requirements:

*Three Parts:*

1. Create a mobile app which shows users your Business Card, providing the details of your name, interests, contact info, and school credentials.
2. Provide screenshots of skillsets 7-9 (Java programs).
3. Chapter Questions (Chs. 7 & 8)

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application's first interface;
* Screenshot of running application's second interface;
* Screenshots of Skillsets 7-9

#### Assignment Screenshots:

*My Business Card App:*

|   Screenshot of First User Interface:    | Screenshot of Second User Interface:      |
|------------------------------------------|------------------------------------------|
|![UI1](img/i1.png)                      |![UI2](img/i2.png)                      |      



*Skillsets:*

|Skillset 7|Skillset 8|Skillset 9|
|----------|----------|----------|
|![Skillset 7](img/ss7.png)       |![Skillset 5](img/ss8.png)          |![Skillset 6](img/ss9.png)



