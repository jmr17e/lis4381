> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Julia Riccio

### Assignment 3 Requirements:

*Four Parts:*

1. Create a database and provide ERD for a petstore.
2. Create a mobile app which allows users to select which artist they would like to see, and calculate their total price for amount of tickets purchased for said artist.
3. Provide screenshots of skillsets 4-6 (Java programs).
4. Chapter Questions (Chs. 5 & 6)

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of ERD;
* Screenshot of running application's first interface;
* Screenshot of running application's second interface;
* Links to the following files:
    * [a3.mwb](docs/a3.mwb)
    * [a3.sql](docs/a3.sql)

#### Assignment Screenshots:

*My ERD:*

![ERD](img/erd1.png)

*My Concert App:*

|   Screenshot of First User Interface:    | Screenshot of Second User Interface:      |
|------------------------------------------|------------------------------------------|
|![UI1](img/img1.png)                      |![UI2](img/img2.png)                      |      



*Skillsets:*

|Skillset 4|Skillset 5|Skillset 6|
|----------|----------|----------|
|![Skillset 4](img/decisionStructures.png)       |![Skillset 5](img/randomArray.png)          |![Skillset 6](img/methods.png)



