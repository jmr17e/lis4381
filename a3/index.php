<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Julia Riccio">
		<link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 1</title>
		<?php include_once("../css/include_css.php"); ?>
	</head>

	<body>
	<?php include_once("../global/nav.php"); ?>
	<body style="background-image: url(img/bckgrnd1.jpg);">

	<div class="contatiner">
		<div class="starter-template">
			<div class="page-header">
			<?php include_once("global/header.php"); ?>
			</div>
			<p class="text-justify">
			<strong>Four Parts:</strong>
			<br>
1. Create a database and provide ERD for a petstore.
<br>
2. Create a mobile app which allows users to select which artist they would like to see, and calculate their total price for amount of tickets purchased for said artist.
<br>
3. Provide screenshots of skillsets 4-6 (Java programs).
<br>
4. Chapter Questions (Chs. 5 & 6)
<br>

* Course title, your name, assignment requirements, as per A1;
<br>
* Screenshot of ERD;
<br>
* Screenshot of running application's first interface;
<br>
* Screenshot of running application's second interface
<br>


    

	 </p>

			<h4>My ERD</h4>
			<img src="img/erd1.png" class="img-responsive center-block" alt="Petsore ERD">

			<h4>My Concert App</h4>
			<table>
			<tr><td><img src="img/img1.png" class="img-responsive center-block" alt="First user interface"></td><td><img src="img/img2.png" class="img-responsive center-block" alt="Second user interface"></td></tr>
			</table>

			<h4>Skillsets</h4>
			<table>
			<tr><td><img src="img/decisionStructures.png" class="img-responsive center-block" alt="SS4"></td><td><img src="img/randomArray.png" class="img-responsive center-block" alt="SS5"></td><td><img src="img/methods.png" class="img-responsive center-block" alt="SS6"></td></tr>
			</table>

			<?php include_once "global/footer.php"; ?>
			</div>
			</div>

			<?php include_once("../js/include_js.php"); ?>
			</body>
			</html>
