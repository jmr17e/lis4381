<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Julia Riccio">
		<link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 1</title>
		<?php include_once("../css/include_css.php"); ?>
	</head>

	<body>
	<?php include_once("../global/nav.php"); ?>
	<body style="background-image: url(img/bckgrnd1.jpg);">

	<div class="contatiner">
		<div class="starter-template">
			<div class="page-header">
			<?php include_once("global/header.php"); ?>
			</div>
			<p class="text-justify">
			<strong>Two Parts:</strong>
<br>
1. Create a Recipe app with Android Studio
<br>
2. Chapter Questions (Chs. 3 & 4)
<br>
* Course title, your name, assignment requirements, as per A1;
<br>
* Screenshot of running application's first user interface;
<br>
* Screenshot of running application's second user interface.
            <table>

			</p>

			<h4>My Recipe App</h4>
			<table>
			<tr><td><img src="img/r1.png" class="img-responsive center-block" alt="First user interface"></td><td><img src="img/r2.png" class="img-responsive center-block" alt="Second user interface"></td></tr>
			</table>
			 
			

			<h4>Skillsets</h4>
			<table>
			<tr><td><img src="img/EvenOrOdd.png" class="img-responsive center-block" alt="SS1"></td><td><img src="img/largestNumber.png" class="img-responsive center-block" alt="SS2"></td><td><img src="img/ArraysAndLoops.png" class="img-responsive center-block" alt="SS3"></td></tr>
			</table>

			<?php include_once "global/footer.php"; ?>
			</div>
			</div>

			<?php include_once("../js/include_js.php"); ?>
			</body>
			</html>
