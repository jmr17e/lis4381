# LIS4381

## Julia Riccio

### Assignment 4 Requirements:

*Four Parts:*

1. Create Bootstrap carousel and local web portfolio using PHP.
2. Create a form to collect data using client-side validation.
3. Link to local LIS4381 web app
4. Skill sets 10-12

#### README.md file should include the following items:

* Screenshot of main page of web app;
* Screenshot of passed validation;
* Screenshot of failed validation;
* Screenshots of Skillsets 10-12.

#### Assignment Screenshots:

*My Web Profile:*

|Main Page|Passed Validation|Failed Validation|
|----------|----------|----------|
|![Main Page](img/mainpg.png)       |![Passed Validation](img/valid.png)          |![Skillset 6](img/invalid.png)



*Skillsets:*

|Skillset 10|Skillset 11|Skillset 12|
|----------|----------|----------|
|![Skillset 7](img/ss10.png)       |![Skillset 5](img/ss11.png)          |![Skillset 6](img/ss12.png)


[Localhost Link](http://localhost/repos/lis4381/)
