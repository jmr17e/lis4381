> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web App Development

## Julia Riccio

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](./a1/README.md)

    * Install AMPPS

    * Install JDK

    * Install Android Studio and create My First App

    * Provide screenshots of installations

    * Create Bitbucket repo

    * Complete Bitbucket tutorials (bitbucketstationlocations)

    * Provide git command descriptions

2. [A2 README.md](./a2/README.md)

    * Create Healthy Recipes App on Android Studio

    * Provide screenshots of first and second user interface of app

    * Provide screenshots of Skillsets 1-3 (Java programs)


3. [A3 README.md](./a3/README.md)

    * Create a database with ability to forward engineer & provide screenshots of ERD

    * Create an application for a concert that allows the user to pick which artist they would like to see, and calculate the cost of their total tickets

    * Provide screenshots of Skillsets 4-6 (Java Programs)

4. [P1 README.md](./p1/README.md)

    * Create a Business Card app that provides the user with your basic info/school credentials/interests.

    * Provide screenshots of Skillsets 7-9 (Java Programs)
    
5. [A4 README.md](./a4/README.md)

    * Create Bootstrap carousel and local web portfolio using PHP.

    * Create a form to collect data using client-side validation.

    * Link to local LIS4381 web app

    * Provide screnshots of Skillsets 10-12 (Java Programs)

6. [A5 README.md](./a5/README.md)

    * Develop server-side validation of a form in php

    * Develop a form that adds data into a database

    * Provide screenshots of Skillsets 13-15 (Java Programs)

7. [P2 README.md](./p2/README.md)

    * Add edit/delete functionality to petstore database (made in previous assignment)

    * Provide screenshots for editing, deleting records

    * Provide screenshots for RSS feed that was created


