# LIS4381

## Julia Riccio

### Assignment 5 Requirements:

*Four Parts:*

1. Develop server-side validation of a form in php
2. Develop a form that adds data into a database
3. Chapter Questions (chs. 11, 12, & 19)
4. Skill Set 13-15

#### README.md file should include the following items:

* Screenshot of index.php for data in Pet Store SQL table
* Screenshot of server-side data validation
* Local LIS4381 web app: http://localhost/repos/lis4381

#### Assignment Screenshots:

*Pet Store*

Index
|![index.php](img/index.png)
Error
![error.php](img/error.png)



*Skillset 13*
![ss13](img/ss13.png)

*Skillset 14*

Addition Input
![input](img/addition.png)       
Addition Output
![output](img/addition_result.png)          

Division Input
![input](img/division.png)
Division Output
![output](img/division_result.png)

*Skillset 15*

Input
![input](img/ss15.png)
Output
![output](img/ss152.png)


