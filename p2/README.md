# LIS4381

## Julia Riccio

### Project 2 Requirements:

*Four Parts:*

1. Add edit functionality to petstore database
2. Add delete functionality to petstore database
3. Create an RSS Feed
4. Chapter Questions (Chs. 13, 14)

#### README.md file should include the following items:

* Course title, name, assignment requirements
* Screenshot of pet store database running on browser
* Screenshot of editing record in petstore database
* Screenshot of RSS Feed
* Local LIS4381 web app: http://localhost/repos/lis4381

#### Assignment Screenshots:

*Home*

![carousel](img/carousel.png)

*Index*
![index](img/index.png)

*Edit Petstore*

![edit](img/edit_record.png)            


*Failed Validation*

![error](img/error.png)

*Passed Validation*

![updatedrecord](img/updated_record.png)  

*Delete Record Prompt*

![deletedrecord](img/delete_record_prompt.png)   

*Successfully Deleted Record*

![success](img/deleted_record.png)   

*RSS Feed*

![rss](img/rss.png)   


