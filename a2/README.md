> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Julia Riccio

### Assignment 2 Requirements:

*Two Parts:*

1. Create a Recipe app with Android Studio
2. Chapter Questions (Chs. 3 & 4)

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application's first user interface;
* Screenshot of running application's second user interface.

#### Assignment Screenshots:

*My Recipe App:*

|   First User Interface    |   Second User Interface   |
|---|---|
|![First UI](img/r1.png)|![Second UI](img/r2.png)   |





*Skillsets:*

|Skillset 1|Skillset 2|Skillset 3|
|----------|----------|----------|
|![Skillset 1](img/EvenOrOdd.png)       |![Skillset 2](img/largestNumber.png)          |![Skillset 3](img/ArraysAndLoops.png)




