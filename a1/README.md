> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Julia Riccio

### Assignment 1 Requirements:

*Three parts:*

1. Distributed version control with Git and BitBucket
2. Development Installations
3. Chapter Questions (Chs. 1 & 2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation
* Screenshot of running java Hello;
* Screenshot of running Android Studio - My First App
* Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: creates a new Git repository and can initialize a new repository
2. git status: displays state of working directory and staging area
3. git add: marks changes to be included in next commit
4. git commit: used to save your changes to local repository
5. git push: used to upload local repository content to remote repository
6. git pull: used to transfer these commits from local to remote repository
7. git branch: lists all local branches in current repository

#### Assignment Screenshots:

*Screenshot of AMPPS running* [My PHP Installation](http://localhost/cgi-bin/phpinfo.cgi "PHP Installation"):

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jmr17e/bitbucketstationlocations/ "Bitbucket Station Locations")


